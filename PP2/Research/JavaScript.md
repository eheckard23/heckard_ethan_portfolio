#JavaScript
___

While HTML and CSS are great for the structure and style of a webpage, JavaScript is how we can add behavior to the elements on a page, thus creating a more active and exciting experience for the user.

Use the script tag to add JavaScript to your file.

Unlike C#, you will **always** use the *var* keyword when declaring a variable with JavaScript. As far as the names for variables, 2 rules must be followed:

1. Variable names must start with a letter, underscore, or dollar sign.
2. After that, any character can be used as much as you want.

Syntactically, JavaScript and C# are fairly similar. All statments end with semicolons, 2 forward slashes create comments, strings are surrounded by quotes, and variables do not have to have an immediate value set to them.

**Alert** - Takes a string and displays it in a popup dialog in the browser to alert the user about something.

**Functions**Functions are similar to the way methods work in C#. However, declaring a function is a little simpler than declaring a method. 
To start, simply use the keyword function followed by the function name paired with parenthesis. The parenthesis are to hold parameter values that are assigned when the function is called. When the function is actually called in the body, we can pass in arguments. Arguments are the actual values we assign to the original parameters.Within our function, we can declare variables that are considered “local.” Just like C#, local variables can only be used within the function it is declared in. Global variables declared outside of the functions can be used anywhere in the code.**Arrays**To begin creating arrays, use the var keyword followed by the name of the array. After that, use the square brackets to surround the values. Accessing, updating, and checking the size of an array is the same way as the C# language.
**Push***array***.push()** - Adds element to the end of array
*array***.splice(x,y)** - Removes elements starting at position **x**, and continues to remove **y** amount of elements following **x**.
*array***.pop()** - Removes the last element of the array