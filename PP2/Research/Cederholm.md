#Research
####*Dan Cederholm*
I'm pretty new to the whole web side of things and so I am finding out who the "industry professionals" are. After a bit of searching, the name Dan Cederholm seemed rather prevalent. Cederholm is a down-to-earth banjo player from Salem, Massachusetts where he currently works at his studio and blog, SimpleBits.

Cederholm is an amazing web designer most recognized as a co-founder for the designer show-and-tell site, Dribbble. He is an industry leader when it comes to designing for web standards and pushing the boundaries for CSS and SASS. I chose Dan because with all of his talent and knowledge, he doesn't seem to boast, rather he often attends public speaking events like SXSW where he shares that knowledge. He is also the author of numerous books over designing for  web standards with CSS and SASS.

[Visit his blog here](http://simplebits.com/)