var currentMoney;
var convertedMoney;
var inGermany = .92;
var inChina = 6.53;
var inMexico = 17.83;
var myCountry = true;

// user story context
var userInput = prompt("How much money do you have saved up for vacation?" , 100);

// output results
window.alert("Amount of American dollars is " + userInput);
window.alert("Amount of German euros is " + (userInput * inGermany));
window.alert("Amount of Chinese yuan dollars is " + (userInput * inChina));
window.alert("Amount of Mexican pesos is " + Math.floor((userInput * inMexico)));