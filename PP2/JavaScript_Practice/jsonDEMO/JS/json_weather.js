var data = {
	"location" : { 
		"city" : "Orlando" ,
		"country" : "United States" ,
		"region" : "FL" 
	},

	"results" : { 
		"distance" : "mi" ,
		"pressure" : "ins" ,
		"speed" : "mph" ,
		"temperature" : "F" 
	},

	"wind" : { 
		"chill" : 76 ,
		"direction" : 360 ,
		"speed" : 5 
	},

	"atmosphere" : { 
		"humidity" : 48 ,
		"pressure" : 29.65 ,
		"rising" : 0 ,
		"visibility" : 10 
	},

	"astronomy" : { 
		"sunrise" : "7:26 am" ,
		"sunset" : "6:51 pm" 
	},

	"forecast" : [
		{ "code" : 30 , 
		  "date" : "17 Oct 2014" , 
		  "day" : "Friday" , 
		  "high" : 80 , 
		  "low" : 62 , 
		  "text" : "Sunny" },

		{ "code" : 30 , 
		  "date" : "18 Oct 2014" , 
		  "day" : "Saturday" , 
		  "high" : 82 , 
		  "low" : 63 , 
		  "text" : "Partly Cloudy" },

		{ "code" : 30 , 
		  "date" : "19 Oct 2014" , 
		  "day" : "Sunday" , 
		  "high" : 85 , 
		  "low" : 65 , 
		  "text" : "Partly Cloudy"}
	]
}

//test
console.log(data.forecast[2].day);


		
