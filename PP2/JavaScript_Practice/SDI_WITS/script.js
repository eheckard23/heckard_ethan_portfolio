// variables
var currentWeight;
var convertedWeight;
var theMoon = .166;
var onJupiter = 2.364;
var onMercury = .378;

//user input
var currentWeight = prompt("Enter your current weight in pounds: " , "Ex. 145");

// output results
window.alert("Your original weight on Earth was " + currentWeight + " pounds.");
window.alert("Your weight on the Moon is " + (currentWeight * theMoon) + " pounds.");
window.alert("Your weight on Jupiter is " + (currentWeight * onJupiter) + " pounds.");
window.alert("Your weight on Mercury is " + (currentWeight * onMercury) + " pounds.");