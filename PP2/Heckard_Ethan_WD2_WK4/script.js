(function(){

	console.log("Welcome to the Star Wars Quiz Game!");

	// create question constructor
	function Questions(question, choices, answer){
		this.question = question;
		this.choices = choices;
		this.answer = answer;
		this.display = function(choice){
			console.log(this.question + " " + this.choices);
			var userAnswer = prompt("Please enter your choice");
			var userAnswer = userAnswer.toLowerCase();
			choice = userAnswer;
			return choice;
		}
	}

	function Character(firstName, lastName, side, strength){
		this.firstName = firstName;
		this.lastName = lastName;
		this.side = side;
		this.strength = Math.floor((Math.random() * 100) + 1);
	}


	// create array to hold list of questions
	var quizList = new Array();
	quizList[0] = new Questions("Dream Job:", ["1) Military ", " 2) Politician ", " 3) Pilot ", " 4) Racer"]);
	quizList[1] = new Questions("Choose one to describe yourself:", ["1) Leader ", " 2) Follower"]);
	quizList[2] = new Questions("What is your stance on politics?", ["1) Democracy ", " 2) Dictatorship"]);
	quizList[3] = new Questions("Family Relationship", ["1) Good ", " 2) Bad"]);
	quizList[4] = new Questions("Are you the wiser one of your friends?", ["1) Yes ", " 2) No"]);
	quizList[5] = new Questions("Do you have a vehicle?", ["1) Yes ", " 2) No ", " 3) It's my mom's"]);
	quizList[6] = new Questions("If you had a weapon to carry, which would it be?", ["1) Gun ", " 2) Lightsaber ", " 3) Bowcaster"]);
	quizList[7] = new Questions("Do you work better in a group or alone?", ["1) Teammate ", " 2) Lone Wolf"]);
	quizList[8] = new Questions("Any anger issues?", ["1) Yes ", " 2) No ", " 3) Sometimes"]);
	quizList[9] = new Questions("Have you ever lost a body part?", ["1) Yes ", " 2) No"]);
	quizList[10] = new Questions("Do you have any companions?", ["1) Yes ", " 2) No"]);
	quizList[11] = new Questions("Are you good at driving?", ["1) Yes ", " 2) No"]);

	// JSON array to hold characters
	// TODO: Add stats regarding attack
	var rebels = [
		{"firstName" : "Luke", "lastName" : "Skywalker"},
		{"firstName" : "Leia", "lastName" : "Organa"},
		{"firstName" : "Master", "lastName" : "Yoda"},
		{"firstName" : "Han", "lastName" : "Solo"},
		{"firstName" : "Chewbacca", "lastName" : ""},
	]

	var empire = [
		{"firstName" : "Emperor", "lastName" : "Palpatine"},
		{"firstName" : "Kylo", "lastName" : "Ren"},
		{"firstName" : "Darth", "lastName" : "Vader"},
		{"firstName" : "Boba", "lastName" : "Fett"},
		{"firstName" : "Tie Fighter Pilot", "lastName" : ""},
		{"firstName" : "Factory", "lastName" : "Droid"}
	]
	

	
	// Dream Job
	var answer = quizList[0].display();

	switch(answer){
		case "1":
		case "2":
		case "military":
		case "politician":
		// leader / follower
		var answer = quizList[1].display();

			switch(answer){
				case "1":
				case "leader":
				// vehicle?
				var answer = quizList[5].display();

					switch(answer){
						case "1":
						case "yes":
						// group or alone?
						var answer = quizList[7].display();

							switch(answer){
								case "1":
								case "teammate":
								console.log("You're character results:");
								console.log("Tie Fighter Pilot");
								var result = new Character("Tie Fighter", "Pilot", "Empire");
								break;

								case "2":
								case "lone wolf":
								console.log("You're character results:");
								console.log("Boba Fett");
								var result = new Character("Boba", "Fett", "Empire");
								break;

								default:
								console.log("Whoops! That answer was not listed. Hit REFRESH to try again.");
								break;
							}
						break;

						case "2":
						case "no":
						console.log("You're character results:");
						console.log("Factory Droid");
						var result = new Character("Factory", "Droid", "Empire");
						break;

						default:
						console.log("Whoops! That answer was not listed. Hit REFRESH to try again.");
						break;
					}
				break;

				case "2":
				case "follower":
				// family relationship
				var answer = quizList[3].display();
				switch(answer){
					case "1":
					case "2":
					case "good":
					case "bad":
					var answer = quizList[8].display();
					switch(answer){
							case "1":
							case "3":
							case "yes":
							case "sometimes":
							var answer = quizList[9].display();
							switch(answer){
								case "1":
								case "yes":
								console.log("You're character results:");
								console.log("Darth Vader");
								var result = new Character("Darth", "Vader", "Empire");
								break;

								case "2":
								case "no":
								console.log("You're character results:");
								console.log("Kylo Ren");
								var result = new Character("Kylo", "Ren", "Empire");
								break;

								default:
								console.log("Whoops! That answer was not listed. Hit REFRESH to try again.");
								break;

							}
							break;
							case "2":
							case "no":
							console.log("You're character results:");
							console.log("Emperor Palpatine");
							var result = new Character("Emperor", "Palpatine", "Empire");
							break;

							default:
							console.log("Whoops! That answer was not listed. Hit REFRESH to try again.");
							break;
					}
					break;

				}
				break;

				default:
				console.log("Whoops! That answer was not listed. Hit REFRESH to try again.");
				break;
			}			
		break;

		case "3":
		case "4":
		case "pilot":
		case "racer":
		// politics
		var answer = quizList[2].display();

			switch(answer){
				case "1":
				case "democracy":
				// wise
				var answer = quizList[4].display();
				switch(answer){
					case "1":
					case "yes":
					// companions
					var answer = quizList[10].display();
					switch(answer){
						case "1":
						case "yes":
						console.log("You're character results:");
						console.log("Princess Leia");
						var result = new Character("Princess", "Leia", "Rebel");
						break;

						case "2":
						case "no":
						console.log("You're character results:");
						console.log("Master Yoda");
						var result = new Character("Master", "Yoda", "Rebel");
						break;

						default:
						console.log("Whoops! That answer was not listed. Hit REFRESH to try again.");
						break;
					}
					break;

					case "2":
					case "no":
					// weapons
					var answer = quizList[6].display();
					switch(answer){
						case "1":
						case "3":
						case "gun":
						case "bowcaster":
						// driving
						var answer = quizList[11].display();
						switch(answer){
							case "1":
							case "yes":
							console.log("You're character results:");
							console.log("Han Solo");
							var result = new Character("Han", "Solo", "Rebel");
							break;

							case "2":
							case "no":
							console.log("You're character results:");
							console.log("Chewbacca");
							var result = new Character("Chewbacca", "Rebel");
							break;

							default:
							console.log("Whoops! That answer was not listed. Hit REFRESH to try again.");
							break;
						}
						break;

						case "2":
						case "lightsaber":
						console.log("You're character results:");
						console.log("Luke Skywalker");
						var result = new Character("Luke", "Skywalker", "Rebel");
						break;

						default:
						console.log("Whoops! That answer was not listed. Hit REFRESH to try again.");
						break;

					}
					break;

					default:
					console.log("Whoops! That answer was not listed. Hit REFRESH to try again.");
					break;
				}
				break;
				case "2":
				case "dictatorship":
				var answer = quizList[3].display();
				switch(answer){
					case "1":
					case "2":
					case "good":
					case "bad":
					var answer = quizList[8].display();
					switch(answer){
							case "1":
							case "3":
							case "yes":
							case "sometimes":
							var answer = quizList[9].display();
							switch(answer){
								case "1":
								case "yes":
								console.log("You're character results:");
								console.log("Darth Vader");
								var result = new Character("Darth", "Vader", "Empire");
								break;
								case "2":
								case "no":
								console.log("You're character results:");
								console.log("Kylo Ren");
								var result = new Character("Kylo", "Ren", "Empire");
								break;
								default:
								console.log("Whoops! That answer was not listed. Hit REFRESH to try again.");
								break;

							}
							break;
							case "2":
							case "no":
							console.log("You're character results:");
							console.log("Emperor Palpatine");
							var result = new Character("Emperor", "Palpatine", "Empire");
							break;

							default:
							console.log("Whoops! That answer was not listed. Hit REFRESH to try again.");
							break;
					}
					break;
				}
				break;

				default:
				console.log("Whoops! That answer was not listed. Hit REFRESH to try again.");
				break;
			}
		break;

		default:
		console.log("Whoops! That answer was not listed. Hit REFRESH to try again.");
		break;

	}

	//---------Display opposing team's list / Choose opponent to fight--------//

	function listEmpire(){
		for (var i = 0; i < empire.length; i++){
			console.log(i + ") " + empire[i].firstName + " " + empire[i].lastName);
		}
		var choice = prompt("Choose an enemy to fight");

		switch(choice){
		case "0":
		var palpatine = new Character("Emperor" , "Palpatine", "Empire");
		result.strength > palpatine.strength ? console.log("You have defeated the evil emperor!") : 
		result.strength = palpatine.strength ? console.log("It's a draw!") :
		console.log("You were unlucky this time. The emperor has defeated you.");
		break;
		case "1":
		var kylo = new Character("Kylo" , "Ren", "Empire");
		result.strength > kylo.strength ? console.log("You have defeated the evil Kylo Ren!") : 
		result.strength = kylo.strength ? console.log("It's a draw!") :
		console.log("You were unlucky this time. Kylo Ren has defeated you.");
		break;
		case "2":
		var darth = new Character("Darth" , "Vader", "Empire");
		result.strength > darth.strength ? console.log("You have defeated the evil Lord Vader!") : 
		result.strength = darth.strength ? console.log("It's a draw!") :
		console.log("You were unlucky this time. Lord Vader has defeated you.");
		break;
		case "3":
		var boba = new Character("Boba" , "Fett", "Empire");
		result.strength > boba.strength ? console.log("You have defeated the bounty hunter Boba Fett!") : 
		result.strength = boba.strength ? console.log("It's a draw!") :
		console.log("You were unlucky this time. Boba Fett has defeated you.");
		break;
		case "4":
		var pilot = new Character("Tie Fighter" , "Pilot", "Empire");
		result.strength > pilot.strength ? console.log("You have defeated the evil Kylo Ren!") : 
		result.strength = pilot.strength ? console.log("It's a draw!") :
		console.log("You were unlucky this time. The fighter pilot has defeated you.");
		break;
		case "5":
		var droid = new Character("Factory" , "Droid", "Empire");
		result.strength > droid.strength ? console.log("You have defeated the droid!") : 
		result.strength = droid.strength ? console.log("It's a draw!") :
		console.log("You were unlucky this time. The droid has defeated you.");
		break;
		default:
		console.log("Invalid");
		break;
	}
	}

	function listRebels(){
		for (var i = 0; i < rebels.length; i++){
			console.log(i + ") " +rebels[i].firstName + " " + rebels[i].lastName);
		}	
		var choice = prompt("Choose an enemy to fight");

		switch(choice){
		case "0":
		var luke = new Character("Luke" , "Skywalker", "Rebel");
		result.strength > luke.strength ? console.log("You have defeated the Jedi Luke Skywalker!") : 
		result.strength = luke.strength ? console.log("It's a draw!") :
		console.log("You were unlucky this time. Skywalker has defeated you.");
		break;
		case "1":
		var leia = new Character("Leia" , "Organa", "Rebel");
		result.strength > leia.strength ? console.log("You have defeated Princess Leia!") : 
		result.strength = leia.strength ? console.log("It's a draw!") :
		console.log("You were unlucky this time. Princess Leia has defeated you.");
		break;
		case "2":
		var yoda = new Character("Master" , "Yoda", "Rebel");
		result.strength > yoda.strength ? console.log("You have defeated Master Yoda!") : 
		result.strength = yoda.strength ? console.log("It's a draw!") :
		console.log("You were unlucky this time. Master Yoda has defeated you.");
		break;
		case "3":
		var han = new Character("Han" , "Solo", "Rebel");
		result.strength > han.strength ? console.log("You have defeated the rebel Han Solo") : 
		result.strength = han.strength ? console.log("It's a draw!") :
		console.log("You were unlucky this time. Han Solo has defeated you.");
		break;
		case "4":
		var chewbacca = new Character("Chewbacca" , "", "Rebel");
		result.strength > chewbacca.strength ? console.log("You have defeated Chewbacca!") : 
		result.strength = chewbacca.strength ? console.log("It's a draw!") :
		console.log("You were unlucky this time. Chewbacca has defeated you.");
		break;
		default:
		console.log("Invalid");
		break;
	}
	}

	result.side === "Empire" ? listRebels() : listEmpire();

	
	

	console.log("Thank you for playing. Hit REFRESH to play again.");

	

})();

