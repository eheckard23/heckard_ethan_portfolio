#Project & Portfolio II

For this portion of my repository, I am conducting research and experimentation over the use of JavaScript principles. I have created a mini Star Wars-based quiz using the JavaScript console to determine which character from the movies you are most like.

### - Ethan Heckard