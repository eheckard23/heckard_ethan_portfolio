##Final Project
This folder includes the files for my final project using the JavaScript language.
####DESCRIPTION:
For my final project, I am working on a Star Wars based quiz, where the user will use the console to answer questions prompted to them. With each question answered, the questions get more and more specific, eventually leading to the character that most suits them.

Lastly, based on which character they receive, they can choose from a list of enemies to attack. Math expressions will be used to determine who will prevail based on strength and maybe even luck.