<?php
class User{

	public function getUsers($db){
		$sql = "select * from user";

		$st = $db->prepare($sql);

		$st->execute();

		$obj = $st->fetchAll();

		return $obj;
	}

	// ------------------------CREATE ACCOUNT------------------------ //

	public function createUser($db, $fname, $lname, $emailaddress, $username, $password){

		// sql statment to be executed
		$sql = "insert into account_info (fname, lname, emailaddress, username, password) VALUES (:fname, :lname, :emailaddress, :username, :password)";

		// prepares statment to be executed within db
		$st = $db->prepare($sql);

		// execute statement
		// store posted variable values within table fields
		$st->execute(array(':fname'=>$fname, ':lname'=>$lname, ':emailaddress'=>$emailaddress, ':username'=>$username, ':password'=>$password));

	}

	// ------------------------SEND EMAIL TO DATABASE------------------------ //

	public function dbEmail($db, $name, $emailaddress, $subject, $message){

		// sql statement to be executed
		$sql = "insert into email_info (name, emailaddress, subject, message) VALUES (:name, :emailaddress, :subject, :message)";

		// prepares statment to be executed within db
		$st = $db->prepare($sql);

		// execute statement
		// store posted variable values within table fields
		$st->execute(array(':name'=>$name, ':emailaddress'=>$emailaddress, ':subject'=>$subject, ':message'=>$message));

	}

	// ------------------------CHECK IF USERNAME EXISTS IN DB------------------------ //

	public function checkUsername($db, $username){

		// sql statement to be executed
		// checks if username is found anywhere in the db
		$sql = "select * from account_info where username = :username";

		$st = $db->prepare($sql);

		$st->execute(array(':username'=>$username));

		// return row count
		$obj = $st->rowCount();

		return $obj;
	}

	// ------------------------CHECK THAT EMAIL DOESN'T ALREADY EXIST------------------------ //

	public function uniqueEmail($db, $emailaddress){

		// sql statement to be executed
		// checks if email address is found anywhere in the db
		$sql = "select * from account_info where emailaddress = :emailaddress";

		$st = $db->prepare($sql);

		$st->execute(array(':emailaddress'=>$emailaddress));

		// return row count
		$obj = $st->rowCount();

		return $obj;
	}

	// ------------------------CHECK FOR CORRECT PASSWORD------------------------ //

	public function validPassword($db, $username, $password){

		// sql statement to be executed
		// checks if password is found in database
		$match = $password;

		$sql = "select * from account_info where username = :username";

		$st = $db->prepare($sql);

		$st->execute(array(':password'=>$password));

		if($match == $st['password']){

			return true;

		}else{

			return false;

		}
	}

	// ------------------------CHECK FOR VALID PASSWORD------------------------ //

	public function testPassword($db, $password){

		// sql statement to be executed
		// checks if password is found in database

		$sql = "select * from account_info where password = :password";

		$st = $db->prepare($sql);

		$st->execute(array(':password'=>$password));

		$obj = $st->rowCount();

		return $obj;
	}

	// ------------------------CORRECT EMAIL FORMAT------------------------ //

	public function validEmail($emailaddress){

		// pass in email address
		// return false if inccorrect format
		if(!filter_var($emailaddress, FILTER_VALIDATE_EMAIL)){

			return false;

		}else{

			return true;

		}
	}

	// ------------------------CORRECT DOMAIN NAME SYSTEM------------------------ //

	public function validDNS($emailaddress){

		// collect substring starting 1 place past '@'
		$domain = substr($emailaddress, strpos($emailaddress, '@') + 1);
		if (checkdnsrr($domain) !== false){
			return true;
		}else{
			return false;
		}
	}

	// ------------------------DB EMAIL TO GMAIL------------------------ //

	public function sendEmail($name, $emailaddress, $subject, $message){

		$subj = "Contact Form Submitted: " . $subject;
		$txt = $message;
		$headers = 'From: <' . $emailaddress . '>\r\n' .
					'Reply-To: <' . $emailaddress . '>';

		mail('<eheckard23@gmail.com>', $subj, $txt, $headers);

	}

}

?>