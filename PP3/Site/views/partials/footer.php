		<div class="footer">
				<div class="social">
					<ul>
						<li>
							<a href="https://twitter.com/ethan_heckard">
								<img src="assets/images/social/twitter.svg" class="icon" id="twitter" />
							</a>
						</li>
						<li>
							<a href="https://www.linkedin.com/in/ethan-heckard">
								<img src="assets/images/social/linkedin.svg" class="icon" id="linkedin" />
							</a>
						</li>
						<li>
							<a href="https://www.instagram.com/all_white_obama/">
								<img src="assets/images/social/insta.svg" class="icon" id="insta" />
							</a>
						</li>
					</ul>
			</div>
			<p>&copy; 2016 Ethan Heckard.</p>
		</div>

	</body>
</html>