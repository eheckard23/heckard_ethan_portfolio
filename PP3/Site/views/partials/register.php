<section class="contactwrapper" style="height: 100%; top: 0px;">
	
	<h1>REGISTER</h1>
	<p class='errorstyle'><?= $data ?></p>
	<section class="formwrapper">
			<form class="emailform" action="/?action=signup" method="POST">
				<ul>
					<li>
						<input type="text" name="fname" placeholder="first name *" autocomplete="off">
					</li>
					<li>
						<input type="text" name="lname" placeholder="last name *" autocomplete="off">
					</li>
					<li>
						<input type="text" name="emailaddress" placeholder="your email *" autocomplete="off">
					</li>
					<li>
						<input type="text" name="username" placeholder="your username *" autocomplete="off">
						
					</li>
					<li>
						<input type="password" name="password" placeholder="your password *" autocomplete="off">
					</li>
					
					<li>
						<input id="submit-btn" type="submit" name="submit" value="SIGN UP" />
					</li>
				</ul>
			</form>
			<a href="/?action=login"><p>Already have an account?</p></a>
	</section>

</section>

