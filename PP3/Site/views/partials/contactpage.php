<section class="contactwrapper">
	<h1>CONTACT ME</h1>
	<p class='errorstyle'><?= $data ?></p>
	<section class="formwrapper">
			<form class="emailform" action="/?action=form" method="POST">
				<ul>
					<li>
						<input type="text" name="name" placeholder="your name *" autocomplete="off">
					</li>
					<li>
						<input type="text" name="emailaddress" placeholder="your email *" autocomplete="off">
					</li>
					<li>
						<input type="text" name="subject" placeholder="subject" autocomplete="off">
					</li>
					<li>
						<textarea name="message" placeholder="message *"></textarea>
					</li>
					<li>
						<input id="submit-btn" type="submit" name="submit" value="SEND" />
					</li>
				</ul>
			</form>
		</section>
</section>
