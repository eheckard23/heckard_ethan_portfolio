<section class="aboutwrapper">
	<h1>NICE TO MEET YOU</h1></br></br>
	<div class="abimgwrapper">
		<img src="assets/images/portrait_about.jpg" class="abhero">
	</div>
	<div class="content">
		<h4>I am a current student of <a href="http://www.fullsail.edu/">Full Sail University's</a> Web Development program. Originally from McLouth, Kansas, I now live in Winter Park, Florida, working to be a better programmer.</h4>

		<p>Web programming hasn't always been the goal for me. At 12, I received my first guitar and, through years of lessons and self-teaching, I will continue to have a passion for tinkering and experimenting with all types of musical genres.</p>

	</div>
	
</section>

<section id="biocont">
	<div id="copy">
		<p>Web programming hasn't always been the goal for Ethan. At 12, he received his first guitar and, through years of lessons and self-teaching, Ethan's passion for creating and experimenting with a wide range of musical types remains.</p>
	</div>
		
</section>