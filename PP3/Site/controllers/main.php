<?php
// ------------------------INCLUDES------------------------ //
// include the functions for accessing views and performing user functions
include "models/view.php";
include "models/user.php";
include 'views/partials/header.php';
// connect to database once
require_once "models/db.php";
session_start();

$view = new View();
$user = new User();

// if action = something go to that page
// otherwise go to register
if(isset($_GET['action'])){
	$action = $_GET['action'];
}else{
	$action = '';
}

// ------------------------VIEWS------------------------ //
// register or sign up
if($action == ''){

	$view->getView("views/partials/register.php");

}else if($action == 'homepage'){
	// send to homepage
	if(isset($_SESSION['username'])){
	$view->getView("views/partials/nav.php");
	$view->getView("views/partials/homepage.php");
	//$view->getView("views/partials/footer.php");
	}else{
		header('location:?/login.php');
	}
	
}else if($action == 'contact'){

	if(isset($_SESSION['username'])){
	// send to contact page
	$view->getView("views/partials/nav.php");
	// send view along with error data
	$view->getView("views/partials/contactpage.php", $_SESSION['usererror']);
	//$view->getView("views/partials/footer.php");
	$_SESSION['usererror'] = null;

	}else{
		header('location:?/login.php');
	}

}else if($action == 'about'){

	if(isset($_SESSION['username'])){
	// send to about page
	$view->getView("views/partials/nav.php");
	$view->getView("views/partials/about.php");
	//$view->getView("views/partials/footer.php");

	}else{
		header('location:?/login.php');
	}

}else if($action == 'work'){

	if(isset($_SESSION['username'])){
	// send to work page
	$view->getView("views/partials/nav.php");
	$view->getView("views/partials/work.php");
	//$view->getView("views/partials/footer.php");

	}else{
		header('location:?/login.php');
	}

}else if($action == 'register'){

	$view->getView("views/partials/register.php", $_SESSION['usererror']);
	$_SESSION['usererror'] = null;

// ------------------------REGISTER------------------------ //

}else if($action == 'signup'){

		// store posted variables
		$fname = $_POST['fname'];
		$lname = $_POST['lname'];
		$emailaddress = $_POST['emailaddress'];
		$username = $_POST['username'];
		$password = $_POST['password'];

		$required = array('fname', 'lname', 'emailaddress', 'username', 'password');
		$empty = false;

		// check for empty fields
		foreach ($required as $field) {
			if(!isset($_POST[$field]) || empty($_POST[$field])){

			$empty = true;

			}
		}

		if($empty){

			// display error
			$_SESSION['usererror'] = 'all fields are required';
			header('location:/?action=register');
			exit();

		}

		// user validation function
		$validEmail = $user->validEmail($emailaddress);
		$validDNS = $user->validDNS($emailaddress);

		if(!$validDNS || !$validEmail){

			// display error
			$_SESSION['usererror'] = 'please enter a valid email';
			header('location:/?action=register');
			exit();

		}

		// user validation functions
		$validusername = $user->checkUsername($db, $username);
		$uniqueEmail = $user->uniqueEmail($db, $emailaddress);

		if($validusername > 0){
			// username already exists in database
			$_SESSION['usererror'] = 'username in use';
			header('location:/?action=register');
			exit();
		
		}else if($uniqueEmail > 0){
			// email address already exists in database
			$_SESSION['usererror'] = 'email in use';
			header('location:/?action=register');
			exit();

		}else{
			// create user account in database
			$_SESSION['username'] = $username;
			$user->createUser($db, $fname, $lname, $emailaddress, $username, $password);
			header('location:/?action=homepage');
			exit();
		}

}else if($action == 'thanks'){
	// redirect to thanks
	$view->getView("views/partials/nav.php");
	$view->getView("views/partials/thanks.php");
	//$view->getView("views/partials/footer.php");

// ------------------------LOGIN------------------------ //

}else if($action == 'login'){

	$view->getView("views/partials/login.php", $_SESSION['usererror']);
	$_SESSION['usererror'] = null;

}else if($action == 'loggingin'){

	// store posted variables
	$username = $_POST['username'];
	$password = $_POST['password'];

	// user validation functions
	$validusername = $user->checkUsername($db, $username);
	$validpass = $user->testPassword($db, $password);

	if($validusername > 0 && $validpass > 0){

		// user is found access granted
		$_SESSION['username'] = $username;
		header('location:/?action=homepage');
		exit();

	}else{

		// no user found
		$_SESSION['usererror'] = 'username or password is incorrect';
		header('location:/?action=login');
		exit();
	}

}else if($action == 'logout'){

	// remove username and password variables
	session_unset();
	$view->getView("views/partials/login.php");

// ------------------------CONTACT------------------------ //

}else if($action == 'form'){

	// store posted variables
	$name = $_POST['name'];
	$emailaddress = $_POST['emailaddress'];
	$subject = $_POST['subject'];
	$message = $_POST['message'];

	$required = array('name', 'emailaddress', 'subject', 'message');
	$empty = false;

	foreach ($required as $field) {
		if(!isset($_POST[$field]) || empty($_POST[$field])){

			$empty = true;

		}
	}

	if($empty){

		// display error
		$_SESSION['usererror'] = 'all fields are required';
		header('location:/?action=contact');
		exit();

	}

	// user validation function
	$validEmail = $user->validEmail($emailaddress);
	$validDNS = $user->validDNS($emailaddress);

		if(!$validDNS || !$validEmail){

			// display error
			$_SESSION['usererror'] = 'please enter a valid email';
			header('location:/?action=contact');
			exit();

		}

	// send email
	$user->dbEmail($db, $name, $emailaddress, $subject, $message);
	$user->sendEmail($name, $emailaddress, $subject, $message);
	header('location:/?action=thanks');
	exit();

}



?>